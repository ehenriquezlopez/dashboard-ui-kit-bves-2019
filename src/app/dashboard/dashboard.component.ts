import { Component, AfterViewInit, OnInit } from "@angular/core";
import { SafeHtml, DomSanitizer } from "@angular/platform-browser";

import { SimpleMenu } from 'simple-sidenav';


@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"]
})
// export class DashboardComponent implements OnInit {
//   constructor() {}

//   ngOnInit() {}
// }
export class DashboardComponent implements OnInit {
 
  menu: SimpleMenu[] = [
    { "id": "1", "name": "Opcion 1" },
    { "id": "2", "name": "Opcion 2" },
    { "id": "3", "name": "Opcion 3" },
    { "id": "4", "name": "Opcion 4", "icon": "", "menu": [
      { "id": "5", "name": "Supopcion 4.1", "menu": [
        { "id": "6", "name": "Supopcion 4.2" }
      ] },
    ] },
    { "id": "7", "name": "Opcion 5", "icon": "", "menu": [
      { "id": "8", "name": "Supopcion 5.1"},
      { "id": "9", "name": "Supopcion 5.2"},
      { "id": "10", "name": "Supopcion 5.3"}
    ] },
    { "id": "11", "name": "Opcion 6" },
    { "id": "12", "name": "Home" },
    { "id": "13", "name": "UI/KIT" },
    { "id": "14", "name": "ERROR" },
    { "id": "15", "name": "DETALLE" },
    { "id": "16", "name": "REGISTRO" }
  ]


 
  onClick(item: {id: number|string, name: string, icon: string, index: number}) {
   
    if (item!=null) {
      switch (item['id']) {
        case '1':
          this.toggle_sidebar();
          break;
        case '2':
          this.toggle_sidebar();
          break;
        case '3':
          this.toggle_sidebar();
          break;
        case '4':
          
          break;
        case '5':
          
          break;
        case '6':
        this.toggle_sidebar();
          break;
        case '7':
          
          break;
        case '8':
          this.toggle_sidebar();
          break;
        case '8':
          this.toggle_sidebar();
          break;
        case '9':
          this.toggle_sidebar();
          break;
        case '10':
          this.toggle_sidebar();
          break;
        case '11':
          this.toggle_sidebar();
          break;
        case '12':
          this.tabIndex = 0;
        
          this.toggle_sidebar();
          break;
        case '13':
          this.tabIndex = 1;
  
          this.toggle_sidebar();
          break;
        case '14':
          this.tabIndex = 2;
          if (this.tabIndex === 2) {
            this.toggleMicroTwo();
          }
          this.toggle_sidebar();
          break;
        case '15':
          this.tabIndex = 3;
  
          this.toggle_sidebar();
          break;
        case '16':
          this.tabIndex = 4;
  
          this.toggle_sidebar();
          break;
        default:
          break;
      }
    }
   
  } 
  public ngOnInit() {
    // $(document).ready(function() {
    //   $("#sidebar li a.dropdown-toggle").on("click", function() {
    //     $("#sidebar li.dropdown-sub").addClass("dropdown-inactive");
    //     $("#sidebar li.dropdown-sub").removeClass("dropdown-active");
    //     if ($(this.parentNode).hasClass("dropdown-inactive")) {
    //       console.log("TRUE");
    //       $(this.parentNode).addClass("dropdown-active");
    //       // $(this.parentNode).removeClass("dropdown-inactive");
    //     } else {
    //       console.log("FALSE");
    //       // $(this.parentNode).addClass("dropdown-inactive");
    //       // $(this.parentNode).removeClass("dropdown-active");
    //     }
    //   });
    // });
  }
  text =
    "&nbsp &nbsp &nbsp Bolsa de Valores de El Salvador®. Todos los derechos reservado";
  tabIndex = 0;
  onTabClick(index) {
    this.tabIndex = index;
    if (index === 2) {
      this.toggleMicroTwo();
    }

    // cierro el sidebar al dar click en algun link de la pagina
    this.toggle_sidebar();
  }
  title = "micro-front";

  toShow: SafeHtml = "";

  constructor(private sanitizer: DomSanitizer) {}

  // agrego una variable para validar y poder control sobre el toggle del sidebar
  status: boolean = true;
  // Agregamos una funcion en la cual validaremos el estado del sidebar , esta funcion se debe de agregar en el evento (click) para que se cierre automaticamente
  toggle_sidebar() {
    // valido que entra a la funcion
    this.status = !this.status;
    // veo que este realizando el cambio de estado
    console.log(this.status);
  }

  toggle_dropdown() {}
  private loadScript(url: string): void {
    //cargamos el escript que nos ayudara a mostrar a nuestros componentes
    if (document.querySelectorAll(`script[src='${url}']`).length === 0) {
      const script = document.createElement("script");
      script.onload = function() {
        // do stuff with the script
      };
      script.src = url;
      document.head.appendChild(script);
    }
  }

  public toggleMicroOne() {
    // Componente compilado como un microcomponete de Login
    // if (!this.toShow) {
    console.log("alerta");
    this.loadScript("elements/micro-one.js");
    this.toShow = this.sanitizer.bypassSecurityTrustHtml(`<micro-one>
      <div class="loader-05"></div>
      </micro-one>`);
    // } else {
    //   this.toShow = '';
    // }
  }

  public toggleMicroTwo() {
    // Componente compilado como un microcomponete de Error
    // if (!this.toShow) {
    console.log("alerta");
    this.loadScript("elements/micro-two.js");
    this.toShow = this.sanitizer.bypassSecurityTrustHtml(`<micro-two>
      <div class="loader-05"></div>
      </micro-two>`);
    // } else {
    //   this.toShow = '';
    // }
  }
}
